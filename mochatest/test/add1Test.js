var assert = require('chai').assert;
var add = require('../add')

describe('add', () => {
  it('should return addn', ()=>{
    var result = add(5)
    assert.equal(result, '7')
  });

  it('should return typed no.', () => {
    var result = add(5)
    assert.typeOf(result, 'number')

  })
})