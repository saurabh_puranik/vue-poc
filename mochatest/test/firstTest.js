var assert = require('chai').assert;
var first = require('../first')

describe('first', () => {
  it('should return hello world', ()=>{
    assert.equal(first(), 'hello world')
  })
})