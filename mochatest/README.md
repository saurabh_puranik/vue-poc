To Run the test case 

1. In a root folder of project, run 'npm run test' using terminal to run the test case. 
2. Run 'npm run coverage' to see the test coverage in terminal console
2. For html based test coverage, go to coverage -> lcov-report -> mochatest -> index.html

