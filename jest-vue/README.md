To Run the test case 

1. Open a project in IDE
2. In a root folder of project, run 'npm run test:unit' using terminal. The test coverage will be visible on the terminal console
3. For html based test coverage, go to coverage -> lcov-report -> src -> components -> index.html


# testing-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
