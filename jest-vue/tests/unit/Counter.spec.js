
import Counter from '../../src/components/Counter.vue'
import { mount } from '@vue/test-utils'

describe('Counter.vue', () => {
  test('inc ctr val when clicked', () => {
    const wrapper = mount(Counter)
    for(var i = 0 ; i < 5 ; i++) {
      console.log(i)
      expect(wrapper.text()).toContain('counter:'+i)
      wrapper.find('button').trigger('click')
    }
  
    // expect(wrapper.text()).toContain('counter:1')
  })
})