1. Open a project in IDE
2. In a root folder of project, run 'npm run test:unit' using terminal. The test coverage will be visible on the terminal console
3. For html based test coverage, go to coverage -> lcov-report -> src -> components -> index.html
